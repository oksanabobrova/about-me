Доброго времени суток! Меня зовут Оксана Боброва, мне 27 лет. Я работаю контент-редактором сайта https://ru.expertprice.com/
Для начала немного о себе: родилась и выросла в Москве, успешно закончила МГУ по специальности биолога. Во время обучения подрабатывала копирайтингом, а в дальнейшем перешла к работе контент-менеджера. В этом посте я расскажу вам о том, кто такой контент-менеджер и чем он занимается:)

Контент-менеджер (content manager) – специалист по созданию и курированию контента. В некоторых фирмах он только обслуживает проекты, а где-то сам создает их, отвечает за рекламу и продвижение. Объединяет всех контент-менеджеров одна общая задача – обеспечивать свежесть, актуальность и информативность материала на сайте.

Когда мы говорим об этой профессии, первое, что приходит в голову – написание и обработка текстов для сайтов в интернете. В действительности же должностные обязанности специалиста куда более разнообразны. В зависимости от маркетинговых задач компании, контент-менеджер может брать на себя функции веб-мастера, маркетолога, копирайтера или редактора.

Чтобы посетители сайта лучше воспринимали текст, в нем должны присутствовать визуальные примеры. Чтобы дополнить и украсить материал, абстрактные изображения из сети не подойдут. Нужны живые картинки, иллюстрирующие именно этот конкретный участок статьи. Вместо затертых изображений из фотостоков грамотный контент-менеджер находит оригинальную графику или создает ее сам с помощью редакторов.

Тексты, которые создает специалист, предназначены не только для людей, но и для поисковых роботов. Чтобы вывести сайт в топ, составляется задание с указанием количества знаков и ключевых слов в статье. Иногда контент-менеджер сам определяет объем материала и собирает семантическое ядро, но чаще всего этим занимаются сеошники. Задачи контентщика по оптимизации следующие:
Вписать ключи и равномерно распределить их в тексте.
Структурировать статью, добавить маркированные списки и заголовки.
Проверить статью на уникальность, водность и переспам.

Структурированная статья лучше ранжируется поисковыми системами, а еще ее удобно читать и воспринимать посетителям сайта. Когда пользователь видит простыню текста, у него возникает желание закрыть страницу и поискать информацию там, где с подачей материала всё в порядке. Контент-менеджер сайта, чтобы у читателя при виде текста не начинали болеть глаза оптимизирует текст метатегами.
Спасибо за внимание, продолжение читайте в моих последующих блогпостах!
